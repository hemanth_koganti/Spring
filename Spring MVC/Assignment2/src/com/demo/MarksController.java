package com.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MarksController {

	@RequestMapping(value="/marks.htm",method=RequestMethod.POST)
	public ModelAndView addMarks(@ModelAttribute("marks") Marks marks)
	{
		ModelAndView mav=new ModelAndView("output");
		mav.addObject("sum", marks.getEnglish()+marks.getMaths()+marks.getScience());
		return mav;
	}
}
