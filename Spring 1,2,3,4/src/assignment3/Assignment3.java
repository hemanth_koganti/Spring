package assignment3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Assignment3 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans3.xml");
		Studentdetails sd = (Studentdetails)context.getBean("studentdetails");
		sd.getAllDetails();
		sd.getDetails("13BCE0441");
		
		
	}

}
