package assignment3;

import java.util.Date;

public class Logging {
	
	Date date = new Date();
	
	public void afterGetDetails(long time){
		System.out.println("getDetails() execution time : " + time +"\n");
	}
	
	public void afterGetAllDetails(long time){
		
		System.out.println("getAllDetails() execution time : " + time +"\n");
	}
	
}
