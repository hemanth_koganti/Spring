package assignment3;

import java.util.List;

public class Studentdetails {
	private List<Student> sd;

	public List<Student> getSd() {
		return sd;
	}

	public void setSd(List<Student> sd) {
		this.sd = sd;
	}
	
	public long getDetails(String sid){
		long start = System.currentTimeMillis();
		for(Student s1 : sd){
			if(sid.equals(s1.getStudentId())){
				System.out.println("Student Id : "+s1.getStudentId()+"\n"+"Student Name : "+s1.getStudentName()+"\n"+"Student Address :"+s1.getStudentAddress()+"\n");
			}
		}
		long Elapsedtime = System.currentTimeMillis() - start;
		return Elapsedtime;
		
	}
	
	public long getAllDetails(){
		long start = System.currentTimeMillis();
		for(Student s1 : sd){
			System.out.println("Student Id : "+s1.getStudentId()+"\n"+"Student Name : "+s1.getStudentName()+"\n"+"Student Address :"+s1.getStudentAddress()+"\n");
		}
		long Elapsedtime = System.currentTimeMillis() - start;
		return Elapsedtime;
	}

}
