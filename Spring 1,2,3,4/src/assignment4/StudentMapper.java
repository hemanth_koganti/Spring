package assignment4;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StudentMapper implements RowMapper<Student> {
	
	public Student mapRow(ResultSet rs,int rowNum) throws SQLException {
		Student student = new Student();
		student.setStudentId(rs.getString("StudentId"));
		student.setStudentName(rs.getString("StudentName"));
		student.setStudentAddress(rs.getString("StudentAddress"));
		
		return student;
	}

}
