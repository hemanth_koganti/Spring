package assignment4;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

public class StudentJDBCTemplate {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource); 
	}
	
	public Student getDetails(String id) {
		String SQL = "select * from Student where StudentId = ?";
		Student student = jdbcTemplateObject.queryForObject(SQL, new Object[]{id},new StudentMapper());
		return student;
	}

	
	public List<Student> getAllDetails() {
		String SQL = "select * from Student";
		List <Student> students = jdbcTemplateObject.query(SQL, new StudentMapper());
		return students;
	}

}
