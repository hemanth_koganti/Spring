package assignment4;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
   public static void main(String[] args) {
      ApplicationContext context = new ClassPathXmlApplicationContext("Beans4.xml");

      StudentJDBCTemplate studentJDBCTemplate = 
         (StudentJDBCTemplate)context.getBean("studentJDBCTemplate");

      System.out.println("------Listing Multiple Records--------" );
      List<Student> students = studentJDBCTemplate.getAllDetails();
      
      for (Student record : students) {
         System.out.print("ID : " + record.getStudentId() );
         System.out.print(", Name : " + record.getStudentName());
         System.out.println(", Address : " + record.getStudentAddress());
      }

      System.out.println("----Listing Record with ID = 13BEC0682 -----" );
      Student student = studentJDBCTemplate.getDetails("13BEC0682");
      System.out.print("ID : " + student.getStudentId() );
      System.out.print(", Name : " + student.getStudentName());
      System.out.println(", Address : " + student.getStudentAddress());
   }
}
