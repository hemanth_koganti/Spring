package assignment1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Assignment1 {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans1-1.xml");
		Movie obj = (Movie) context.getBean("movie");
		obj.getMovieId();
		obj.getMovieName();
		obj.getMovieActor();
	}

}
