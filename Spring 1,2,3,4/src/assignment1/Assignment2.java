package assignment1;

import org.springframework.context.annotation.*;
import org.springframework.context.ApplicationContext;

public class Assignment2 {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(MovieConfig.class);
		Movie movie = context.getBean(Movie.class);
		movie.setMovieId("34543");
		movie.setMovieName("JLK");
		movie.setMovieActor("NTR");
		movie.getMovieId();
		movie.getMovieName();
		movie.getMovieActor();
	}

}
