package assignment2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Assignment1 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans2-1.xml");
		Student student1 = (Student)context.getBean("student1");
		student1.showinfo();
		Student student2 = (Student)context.getBean("student2");
		student2.showinfo();
	}

}
