package assignment2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Playerconfig {
	@Bean
	public Player player1(){
		Player p1 = new Player();
		p1.setInfo("18", "Virat Kohli",country1());
		return p1;
	}
	@Bean
	public Player player2(){
		Player p2 = new Player();
		p2.setInfo("7", "MS Dhoni",country1());
		return p2;
	}
	@Bean
	public Player player3(){
		Player p3 = new Player();
		p3.setInfo("10", "Sachin Tendulkar",country1());
		return p3;
	}
	@Bean
	public Player player4(){
		Player p4 = new Player();
		p4.setInfo("66", "Graeme Swann",country2());
		return p4;
	}
	@Bean
	public Player player5(){
		Player p5 = new Player();
		p5.setInfo("9", "James Anderson",country2());
		return p5;
	}
	@Bean
	public Country country1(){
		Country c1 = new Country();
		c1.setCountryId("IND");
		c1.setCountryName("India");
		return c1;
	}
	@Bean
	public Country country2(){
		Country c2 = new Country();
		c2.setCountryId("ENG");
		c2.setCountryName("England");
		return c2;
	}
	
}
