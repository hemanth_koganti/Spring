package assignment2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Assignment2 {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Playerconfig.class);
		Player plyr1 = context.getBean("player1",Player.class);
		plyr1.getInfo();
		Player plyr2 = context.getBean("player2", Player.class);
		plyr2.getInfo();
		Player plyr3 = context.getBean("player3", Player.class);
		plyr3.getInfo();
		Player plyr4 = context.getBean("player4", Player.class);
		plyr4.getInfo();
		Player plyr5 = context.getBean("player5", Player.class);
		plyr5.getInfo();
		
	}

}
