package assignment2;

import java.util.*;

public class Student {
	private String studentId;
	private String studentName;
    private List<Test> t;
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public List<Test> getT() {
		return t;
	}
	public void setT(List<Test> t) {
		this.t = t;
	}
	
	public void showinfo(){
		System.out.println("\nStudent ID:" + getStudentId() + "\n" +  "Student Name:" + getStudentName() + "\n");
        for( Test t1 : t){
        System.out.println("TestId:" + t1.getTestId() + "\n"+ "Test Title: " + t1.getTestTitle() + "\n"  + "Test Marks:"+ t1.getTestMarks() + "\n");
      }
    }

}
