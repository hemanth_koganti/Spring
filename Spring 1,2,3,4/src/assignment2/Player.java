package assignment2;

public class Player {
	private String playerId;
	private String playerName;
	private Country country;
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	
	public void setInfo(String playerId, String playerName, Country country){
		setPlayerId(playerId);
		setPlayerName(playerName);
		setCountry(country);	
	}
	
	public void getInfo(){
		System.out.println("Player Id : " + getPlayerId());
		System.out.println("Player Name : " + getPlayerName());
		System.out.println("Country Id : " + country.getCountryId());
		System.out.println("Country Name : " + country.getCountryName() + "\n\n");	
	}

}
